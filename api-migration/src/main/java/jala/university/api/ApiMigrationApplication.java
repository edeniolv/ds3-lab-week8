package jala.university.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class ApiMigrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiMigrationApplication.class, args);
    }
}
