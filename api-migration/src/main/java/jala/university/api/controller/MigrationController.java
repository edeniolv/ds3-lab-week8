package jala.university.api.controller;

import jala.university.api.model.dto.ExceptionDTO;
import jala.university.api.model.dto.MigrationDTO;
import jala.university.api.model.service.MigrationService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log
@RestController
@RequestMapping("/api/migrate")
public class MigrationController {

    private final MigrationService service;

    @Autowired
    public MigrationController(MigrationService service) {
        this.service = service;
    }

    @GetMapping("/all")
    public ResponseEntity<Object> migrateAll() {
        try {
            long quantityClient = service.migrateAllClient().size();
            long quantityDelivery = service.migrateAllDelivery().size();

            log.info(String.format("%d clients records were migrated", quantityClient));
            log.info(String.format("%d delivers records were migrated", quantityDelivery));

            return ResponseEntity.ok().body(
                new MigrationDTO("Success", quantityClient, quantityDelivery)
            );
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body(
                new ExceptionDTO("Failed", e.getMessage())
            );
        }
    }
}
