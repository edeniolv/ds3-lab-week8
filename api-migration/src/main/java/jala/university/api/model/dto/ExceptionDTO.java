package jala.university.api.model.dto;

public record ExceptionDTO(String status, String error) { }
