package jala.university.api.model.dto;

public record MigrationDTO(String status, long quantityClient, long quantityDelivery) { }
