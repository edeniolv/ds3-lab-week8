package jala.university.api.model.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.io.Serializable;

@Data
@Document("clients")
public class Client implements Serializable {
    @MongoId
    private String id;
    private String name;
    private String email;
    private String phone;
}
