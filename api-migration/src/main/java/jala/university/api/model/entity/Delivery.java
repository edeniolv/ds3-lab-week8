package jala.university.api.model.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.math.BigDecimal;

@Data
@Document("delivery")
public class Delivery {
    @MongoId
    private String id;
    private String code;
    private BigDecimal amount;
    @DBRef
    private Client client;
}
