package jala.university.api.model.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class ClientJdbcRepository {

    private final JdbcTemplate jdbc;

    @Autowired
    public ClientJdbcRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    public List<Map<String, Object>> selectAll() {
        return jdbc.queryForList("SELECT name, email, phone FROM clients");
    }

    public Map<String, Object> selectById(int id) {
        return jdbc.queryForMap("SELECT name, email, phone FROM clients WHERE id = ?", id);
    }
}
