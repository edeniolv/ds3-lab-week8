package jala.university.api.model.repository;

import jala.university.api.model.entity.Client;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientMongoRepository extends MongoRepository<Client, String> {
    Boolean existsClientByEmail(String email);
    Client findClientByEmail(String email);
}
