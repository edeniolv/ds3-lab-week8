package jala.university.api.model.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class DeliveryJdbcRepository {

    private final JdbcTemplate jdbc;

    @Autowired
    public DeliveryJdbcRepository(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    public List<Map<String, Object>> selectAll() {
        return jdbc.queryForList("SELECT code, amount, client_fk FROM delivery");
    }
}
