package jala.university.api.model.repository;

import jala.university.api.model.entity.Delivery;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DeliveryMongoRepository extends MongoRepository<Delivery, String> { }
