package jala.university.api.model.service;

import jala.university.api.model.entity.Client;
import jala.university.api.model.entity.Delivery;
import jala.university.api.model.repository.ClientJdbcRepository;
import jala.university.api.model.repository.ClientMongoRepository;
import jala.university.api.model.repository.DeliveryJdbcRepository;
import jala.university.api.model.repository.DeliveryMongoRepository;
import jala.university.api.utils.MapperImpl;
import jala.university.api.utils.Mapper;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Log
@Service
public class MigrationService {

    private final ClientJdbcRepository clientJdbc;
    private final DeliveryJdbcRepository deliveryJdbc;
    private final ClientMongoRepository clientMongo;
    private final DeliveryMongoRepository deliveryMongo;
    private final Mapper<Client> clientMapper;
    private final Mapper<Delivery> deliveryMapper;

    @Autowired
    public MigrationService(
        ClientJdbcRepository clientJdbc,
        DeliveryJdbcRepository deliveryJdbc,
        ClientMongoRepository clientMongo,
        DeliveryMongoRepository deliveryMongo
    ) {
        this.clientJdbc = clientJdbc;
        this.deliveryJdbc = deliveryJdbc;
        this.clientMongo = clientMongo;
        this.deliveryMongo = deliveryMongo;
        this.clientMapper = new MapperImpl<>(Client.class);
        this.deliveryMapper = new MapperImpl<>(Delivery.class);
    }

    public List<Client> migrateAllClient() {
        List<Map<String, Object>> clients = clientJdbc.selectAll();
        log.info(String.format("%d clients records were read", clients.size()));

        for (Map<String, Object> clientMap : clients) {
            Client client = getClientFromMap(clientMap);
            log.info(String.format("Added or Recovered client: %s", client.getId()));
        }

        return clientMongo.findAll();
    }

    public List<Delivery> migrateAllDelivery() {
        List<Map<String, Object>> delivers = deliveryJdbc.selectAll();
        log.info(String.format("%d delivers records were read", delivers.size()));

        for (Map<String, Object> deliveryMap : delivers) {
            String foreign = getForeignKey(deliveryMap.keySet());

            if (foreign == null) {
                log.severe("Foreign key not found");
                break;
            }

            Map<String, Object> clientMap = getMapFromForeign(deliveryMap, foreign);
            Client client = getClientFromMap(clientMap);
            Delivery delivery = getDeliveryFromMap(deliveryMap, foreign, client);

            log.info(String.format("Added delivery: %s", delivery.getId()));
        }

        return deliveryMongo.findAll();
    }

    private Delivery getDeliveryFromMap(Map<String, Object> map, String key, Client client) {
        map.remove(key);
        map.put(key.replace("_fk", ""), client);

        return deliveryMongo.save(deliveryMapper.toObject(map));
    }

    private Client getClientFromMap(Map<String, Object> map) {
        Client client = clientMapper.toObject(map);

        if (Boolean.FALSE.equals(clientMongo.existsClientByEmail(client.getEmail()))) {
            return clientMongo.save(client);
        }

        return clientMongo.findClientByEmail(client.getEmail());
    }

    private Map<String, Object> getMapFromForeign(Map<String, Object> map, String key) {
        int index = (Integer) map.get(key);
        return clientJdbc.selectById(index);
    }

    private String getForeignKey(Set<String> keys) {
        for (String key : keys) {
            if (key.contains("_fk")) {
                return key;
            }
        }

        return null;
    }
}
