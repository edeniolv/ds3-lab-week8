package jala.university.api.utils;

import java.util.Map;

public interface Mapper<T> {
    T toObject(Map<String, Object> source);
}
