package jala.university.api.utils;

import lombok.extern.java.Log;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.logging.Level;

@Log
public class MapperImpl<T> implements Mapper<T> {

    private final Class<T> clazz;

    public MapperImpl(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public T toObject(Map<String, Object> source) {
        try {
            T entity = clazz.getDeclaredConstructor().newInstance();

            for (Method method : clazz.getDeclaredMethods()) {
                if (method.getName().startsWith("set")) {
                    String field = getField(method.getName());
                    method.invoke(entity, source.get(field));
                }
            }

            return entity;
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed map to object");
        }

        return null;
    }

    private String getField(String value) {
        return value.substring(3).toLowerCase();
    }
}
