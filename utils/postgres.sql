-- Adminer 4.8.1 PostgreSQL 16.2 (Debian 16.2-1.pgdg120+2) dump

DROP TABLE IF EXISTS "clients";
DROP SEQUENCE IF EXISTS clients_id_seq;
CREATE SEQUENCE clients_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 20 CACHE 1;

CREATE TABLE "public"."clients" (
    "id" integer DEFAULT nextval('clients_id_seq') NOT NULL,
    "name" character varying(80) NOT NULL,
    "email" character varying(100) NOT NULL,
    "phone" character varying(11) NOT NULL,
    CONSTRAINT "clients_pk" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "clients_email_index" ON "public"."clients" USING btree ("email");

CREATE INDEX "clients_name_index" ON "public"."clients" USING btree ("name");

TRUNCATE "clients";
INSERT INTO "clients" ("id", "name", "email", "phone") VALUES
(1,	'Pedro Souza',	'pedro.souza@gmail.com',	'46917904095'),
(2,	'Maria Santos',	'maria.santos@yahoo.com',	'11987654321'),
(3,	'Ana Oliveira',	'ana.oliveira@hotmail.com',	'21987654321'),
(4,	'Paulo Silva',	'paulo.silva@outlook.com',	'31987654321'),
(5,	'Carlos Lima',	'carlos.lima@gmail.com',	'41987654321'),
(6,	'Beatriz Pereira',	'beatriz.pereira@yahoo.com',	'51987654321'),
(7,	'Lucas Fernandes',	'lucas.fernandes@hotmail.com',	'61987654321'),
(8,	'Luiza Gomes',	'luiza.gomes@outlook.com',	'71987654321'),
(9,	'Pedro Rocha',	'pedro.rocha@gmail.com',	'81987654321'),
(10,	'Fernanda Almeida',	'fernanda.almeida@yahoo.com',	'91987654321'),
(11,	'João Silva',	'joao.silva@hotmail.com',	'21987654322'),
(12,	'João Santos',	'joao.santos@outlook.com',	'31987654322'),
(13,	'Ana Luciana',	'ana.luciana@gmail.com',	'41987654322'),
(14,	'Paulo Souza',	'paulo.souza@yahoo.com',	'51987654322'),
(15,	'Carlos Marc',	'carlos.marc@hotmail.com',	'61987654322'),
(16,	'Beatriz Oliveira',	'beatriz.olveira@outlook.com',	'71987654322'),
(17,	'Lucas Marques',	'lucas.marques@gmail.com',	'81987654322'),
(18,	'Luiza Maria',	'luiza.maria@yahoo.com',	'91987654322'),
(19,	'Pedro Rocha',	'pedro.rocha@hotmail.com',	'21987654323'),
(20,	'Fernanda Junior',	'fernanda.junior@outlook.com',	'31987654323');

DROP TABLE IF EXISTS "delivery";
DROP SEQUENCE IF EXISTS delivery_id_seq;
CREATE SEQUENCE delivery_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 40 CACHE 1;

DROP SEQUENCE IF EXISTS delivery_client_fk_seq;
CREATE SEQUENCE delivery_client_fk_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."delivery" (
    "id" integer DEFAULT nextval('delivery_id_seq') NOT NULL,
    "code" character varying(5) NOT NULL,
    "amount" numeric NOT NULL,
    "client_fk" integer DEFAULT nextval('delivery_client_fk_seq') NOT NULL,
    CONSTRAINT "delivery_pk" PRIMARY KEY ("id"),
    CONSTRAINT "order_unique" UNIQUE ("code")
) WITH (oids = false);

TRUNCATE "delivery";
INSERT INTO "delivery" ("id", "code", "amount", "client_fk") VALUES
(1,	'00129',	9557.94,	6),
(2,	'00258',	2631.33,	2),
(3,	'00387',	3754.12,	3),
(4,	'00416',	4866.91,	4),
(5,	'00545',	5979.70,	5),
(6,	'00674',	7092.49,	6),
(7,	'00803',	8205.28,	7),
(8,	'00932',	9318.07,	8),
(9,	'01061',	10430.86,	9),
(10,	'01190',	11543.65,	10),
(11,	'01319',	12656.44,	11),
(12,	'01448',	13769.23,	12),
(13,	'01577',	14882.02,	13),
(14,	'01706',	15994.81,	14),
(15,	'01835',	17107.60,	15),
(16,	'01964',	18220.39,	16),
(17,	'02093',	19333.18,	17),
(18,	'02222',	20445.97,	18),
(19,	'02351',	21558.76,	19),
(20,	'02480',	22671.55,	20),
(21,	'02609',	23784.34,	1),
(22,	'02738',	24897.13,	2),
(23,	'02867',	26009.92,	3),
(24,	'02996',	27122.71,	4),
(25,	'03125',	28235.50,	5),
(26,	'03254',	29348.29,	6),
(27,	'03383',	30461.08,	7),
(28,	'03512',	31573.87,	8),
(29,	'03641',	32686.66,	9),
(30,	'03770',	33799.45,	10),
(31,	'03899',	34912.24,	11),
(32,	'04028',	36025.03,	12),
(33,	'04157',	37137.82,	13),
(34,	'04286',	38250.61,	14),
(35,	'04415',	39363.40,	15),
(36,	'04544',	40476.19,	16),
(37,	'04673',	41588.98,	17),
(38,	'04802',	42701.77,	18),
(39,	'04931',	43814.56,	19),
(40,	'05060',	44927.35,	20);

ALTER TABLE ONLY "public"."delivery" ADD CONSTRAINT "delivery_clients_id_fk" FOREIGN KEY (client_fk) REFERENCES clients(id) NOT DEFERRABLE;

-- 2024-04-23 22:57:26.311941+00
